package softagi.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity
{
    TextView result_txt;

    String op;
    int first,second;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        result_txt = findViewById(R.id.result_txt);
    }

    public void buttons(View view)
    {
        result_txt.append(((Button) view).getText());
    }

    public void op(View view)
    {
        String f = result_txt.getText().toString();

        if (f.length() == 0)
        {
            Toast.makeText(Main2Activity.this , "please enter number firstly .." , Toast.LENGTH_SHORT).show();
        } else
        {
            first = Integer.parseInt(f);

            op = ((Button) view).getText().toString();
            result_txt.setText("");
        }
    }

    public void equal(View view)
    {
        String s = result_txt.getText().toString();

        if (s.length() == 0)
        {
            Toast.makeText(Main2Activity.this, "please enter second number ..", Toast.LENGTH_SHORT).show();
        } else
        {
            second = Integer.parseInt(s);

            switch (op)
            {
                case "+":
                    result_txt.setText(String.valueOf(first + second));
                    break;
                case "-":
                    result_txt.setText(String.valueOf(first - second));
                    break;
                case "*":
                    result_txt.setText(String.valueOf(first * second));
                    break;
                case "/":
                    result_txt.setText(String.valueOf(first / second));
                    break;
            }
        }
    }

    public void defaultc(View view)
    {
        result_txt.setText("");
    }
}