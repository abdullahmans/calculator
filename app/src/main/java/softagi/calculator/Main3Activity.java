package softagi.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Main3Activity extends AppCompatActivity
{
    TextView result_txt;
    double first = 0,second;
    String op;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        result_txt = findViewById(R.id.result_txt);
    }

    public void defaultc(View view)
    {
        result_txt.setText("");
    }

    public void buttons(View view)
    {
        result_txt.append(((Button) view).getText());
    }

    public void op(View view)
    {
        String f = result_txt.getText().toString();

        if (f.length() == 0)
        {
            //Toast.makeText(Main3Activity.this, "please enter number firstly ..", Toast.LENGTH_SHORT).show();
        } else
            {
                first = Double.parseDouble(f);
                result_txt.setText("");
                op = ((Button) view).getText().toString();
            }
    }

    public void equal(View view)
    {
        String s = result_txt.getText().toString();


        second = Double.parseDouble(s);

        switch (op)
        {
            case "+" :
                result_txt.setText(String.valueOf(first + second));
                break;
            case "-" :
                result_txt.setText(String.valueOf(first - second));
                break;
            case "*" :
                result_txt.setText(String.valueOf(first * second));
                break;
            case "/" :
                result_txt.setText(String.valueOf(first / second));
                break;
        }
    }
}
